using Gtk;
int ui_init(){
	builder = new Builder ();
	try {
		builder.add_from_file ("window.ui");
		builder.connect_signals (null);
		
		window = builder.get_object ("window") as Window;
		searchModel = builder.get_object ("search_model") as Gtk.ListStore;
		search = builder.get_object ("search") as SearchEntry;
		text_viewer = builder.get_object("text_viewer") as TextView;
		text_buffer = builder.get_object("text_buffer") as TextBuffer;
		//completion
		var entrycompletion = new EntryCompletion();
        entrycompletion.set_model(searchModel);
        entrycompletion.set_text_column(0);
        entrycompletion.set_popup_completion(true);
        search.set_completion(entrycompletion);
        
        clb = window.get_clipboard(Gdk.SELECTION_CLIPBOARD);
        
        text_viewer.set_wrap_mode(WrapMode.WORD);
        text_viewer.buffer.paste_done.connect(paste_done);
        
		window.destroy.connect(Gtk.main_quit);
		window.show_all();
	} catch (Error e) {
        stderr.printf("Could not load UI: %s\n", e.message);
        return 1;
    }
    return 0;
}
