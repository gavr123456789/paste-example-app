#!/usr/bin/vala
    //print(@"$(args[0])");
    var directory = File.new_for_path (".");
    var enumerator = directory.enumerate_children (FileAttribute.STANDARD_NAME, 0);
    FileInfo file_info;
    string[] files = {};
    while ((file_info = enumerator.next_file ()) != null) {
        stdout.printf (@"$(file_info.get_name ())\n");
        string f=file_info.get_name();
        if (f.has_suffix("vala") && !f.has_prefix("compile"))
            files+=file_info.get_name();
    }
    foreach (var file in files) 
        print(@"$file\t");

    var err=""; var output="";

    Process.spawn_command_line_sync("pwd");
    Process.spawn_command_line_sync (@"valac $(string.joinv(" ",files)) 
                                      --pkg gtk+-3.0
                                      --pkg gmodule-2.0",
                                      out err,
                                      out output);
    
    if (err!="") message(@"error while compile: $err\n");
    else message(@"All compiled successfully ^^");
    Process.spawn_command_line_sync("./data");

