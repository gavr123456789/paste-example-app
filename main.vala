//valac data.vala main.vala ui_init.vala slots.vala --pkg gtk+-3.0 --pkg gmodule-2.0 && ./data
using Gtk;
//MAIN
int main (string[] args) {
    Gtk.init (ref args);

	if (ui_init()==0) print("ui inited\n");
	else return 1;
	
	//tgs=new Gdk.Atom[10];
	Gdk.Atom[] tgs={};
	clb.wait_for_targets(out tgs);
	message("Atoms found:\n");
	foreach ( Gdk.Atom tg in tgs ) {
		message(@"TARGET: $(tg.name()) ");
	}
	
    Gtk.main ();
    return 0;
}

