int main(string[] args){
 
  	if(args[1]==null){print("U forget the comment!!!"); return 1;}
  	//if(args[2]==null)args[2]="";

  	string ls_stdout, ls_stderr;
	int ls_status;

	try {
		Process.spawn_command_line_sync ("git add .",
									out ls_stdout,
									out ls_stderr,
									out ls_status);
		stdout.printf ("stdout:\n" + ls_stdout + "stderr:\n" + ls_stderr + @"Status: $ls_status\n");
		
	    Process.spawn_command_line_sync (@"git commit -m \"$(args[1])\" -m\"$(args[2]??"nothing to say")\" ",
									    out ls_stdout,
									    out ls_stderr,
									    out ls_status);
		stdout.printf ("stdout:\n" + ls_stdout + "stderr:\n" + ls_stderr + @"Status: $ls_status\n");
		
	    Process.spawn_command_line_sync ("git push  -u origin master",
									    out ls_stdout,
									    out ls_stderr,
									    out ls_status);
		stdout.printf ("stdout:\n" + ls_stdout + "stderr:\n" + ls_stderr + "Status:");
		
	    if(ls_status==0)print(" All good! ^^\n");
	    else print(" All bad ((\n");

	} catch (SpawnError e) {
		stdout.printf ("Error: %s\n", e.message);
	}
	return 0;
}
