![](https://pp.userapi.com/c851436/v851436811/12d5b5/UMqztp_kyfM.jpg)

# What is this?  
This program demonstrates how to work with the clipboard in GTK.
Это программа демонстрирующая работу с буфером обмена в GTK.  

# And how to use?  
Select the format (*MIME Type*) you want it to accept in search entry, and paste the appropriate format into the text box.  
Выберите формат (*MIME Type*) который вы хотите чтобы она приняла введя его в строку поиска, и вставьте соответствующий формат в текстовое поле.

# How to compile?  
Make executive and run the file `compile_script_vN.vala`.  
Сделайте исполнительным и запустите файл `compile_script_vN.vala` 
