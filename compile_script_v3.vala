#!/usr/bin/vala
using Process;
var output="";var err="";
const string compiler = "valac";const string file_format = "vala";
const string c_args = "--pkg gtk+-3.0 --pkg gmodule-2.0";
string[] files = {};

    spawn_command_line_sync("find ./ -type f -name \"*.vala\" -not -name \"Update*\" -not -name \"compile*\"", out output);
    //  foreach (var file in (owned) files){
    //      files+=file;
    //  }
    string[] temp = output.split("\n"); 
    for (int i = 0; i < temp.length; i++) 
        if (temp[i].has_suffix(file_format)    &&
           !temp[i].has_prefix("compile") &&
           !temp[i].has_prefix("Update")) 
                files+=temp[i];
    
    foreach (var file in files) 
        print(@"$file\n");
    print(compiler+ @" $(string.joinv(" ",files)) "+c_args+"\n");
    spawn_command_line_sync (compiler+@" $(string.joinv(" ",files)) "+c_args,
                                      out output,
                                      out err);
    
    if (err!="") message(@"error while compile: $err\n");
    else message(@"All compiled successfully ^^");
    spawn_command_line_sync("./main");

